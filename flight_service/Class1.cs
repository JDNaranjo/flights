﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace flight_service
{
    class Flight
    {

        private string flightDate;
        private string uniqueCarrier;
        private string airlineID;
        private int flightNumb;
        private string  originAirport;
        private string originCityName;
        private int destAirportID;
        private string destCityName;
        private string CRSDeptTime;
        private string depTime;
        private int depDelay;
        private string CRSArriveTime;
        private int arrDelay;
        private int cancelled;
        private int airTime;

        public Flight(string date, string carrier, string aID, int fNumber, string oAirport, string oCity, int dAirport,
            string dCity, string CRSdep, string dTime, int dDelay, string CRSarr, int aDelay, int cancel, int airT) {

            flightDate = date;
            uniqueCarrier = carrier;
            airlineID = aID;
            flightNumb = fNumber;
            originAirport = oAirport;
            originCityName = oCity;
            destAirportID = dAirport;
            destCityName = dCity;
            CRSDeptTime = CRSdep;
            depTime = dTime;
            depDelay = dDelay;
            CRSArriveTime = CRSarr;
            arrDelay = aDelay;
            cancelled = cancel;
            airTime = airT;

        }
        
        public override string ToString()
        {
            return "Date: "+flightDate+"\n Unique Carrier: "+uniqueCarrier+"\n Airline ID: "+airlineID+"\n " +
                "Fligh Number: "+flightNumb+"\n Origin Airport: "+originAirport+"\n Origin City Name: "+originCityName+
                "\n Destination Airport ID: "+destAirportID+"\n Destination City Name: "+destCityName+"\n "+
                "CRS Departure Time: "+CRSDeptTime+"\n Departure Time: "+depTime+"\n Departure Delay: "+depDelay+"\n " +
                "CRS Arrival Time: "+CRSArriveTime+"\n Arrival Delay: "+arrDelay+"\n Cancelled: "+cancelled+"\n Air Time: " +
                airTime;
        }

        public string getFlightDate() {
            return flightDate;
        }

        public string getUniqueCarrier()
        {
            return uniqueCarrier;
        }

        public string getAirlineID ()
        {
            return airlineID; ;
        }

        public int getFlightNumb() 
        {
            return flightNumb;
        }

        public string getOriginAirport()
        {
            return originAirport;
        }

        public string getOriginCityName()
        {
            return originCityName;
        }

        public int getDestAirportID()
        {
            return destAirportID;
        }

        public string getDestCityName()
        {
            return destCityName;
        }

        public string getCRSDeptTime()
        {
            return CRSDeptTime;
        }

        public string getDepTime()
        {
            return depTime;
        }

        public int getDepDelay()
        {
            return depDelay;
        }

        public string getCRSArriveTime()
        {
            return CRSArriveTime;
        }

        public int getArrDelay()
        {
            return arrDelay;
        }

        public int getCancelled()
        {
            return cancelled;
        }

        public int getAirTime()
        {
            return airTime;
        }

    }
}
