﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace flight_service
{
    public partial class Form1 : Form
    {
        GMapOverlay markerOverlay;
        GMapMarker markerOrigin;
        GMapMarker markerDestination;
        DataTable dt;
        GMapOverlay poligono;
        List<PointLatLng> puntos;

        int filaSeleccionada = 0;
        double latInicial = 40.730610;
        double LngInicial = -73.935242;

        Hashtable usaFlights = new Hashtable();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            markerOverlay = new GMapOverlay("Marcador");
            markerOrigin = new GMarkerGoogle(new PointLatLng(latInicial, LngInicial), GMarkerGoogleType.green_dot);
            markerDestination = new GMarkerGoogle(new PointLatLng(latInicial, LngInicial), GMarkerGoogleType.blue_dot);
            markerOverlay.Markers.Add(markerOrigin);
            markerOverlay.Markers.Add(markerDestination);
            poligono = new GMapOverlay("Poligono");
            puntos = new List<PointLatLng>();


            dt = new DataTable();
            dt.Columns.Add(new DataColumn("Vuelo", typeof(int)));
            dt.Columns.Add(new DataColumn("Numero de Vuelo", typeof(string)));
            dt.Columns.Add(new DataColumn("OriginCity", typeof(string)));
            dt.Columns.Add(new DataColumn("DestinationCity", typeof(string)));
            dt.Columns.Add(new DataColumn("FlightDate", typeof(string)));
            dt.Columns.Add(new DataColumn("DepartureTime", typeof(string)));
            dt.Columns.Add(new DataColumn("ArrivalTime", typeof(string)));
            dt.Columns.Add(new DataColumn("ArrivalDelay", typeof(int)));

            gMapControl.DragButton = MouseButtons.Left;
            gMapControl.CanDragMap = true;
            gMapControl.MapProvider = GMapProviders.GoogleMap;
            gMapControl.Position = new PointLatLng(latInicial, LngInicial);
            gMapControl.MinZoom = 0;
            gMapControl.MaxZoom = 24;
            gMapControl.Zoom = 9;
            gMapControl.AutoScroll = true;

            System.IO.StreamReader file = new System.IO.StreamReader(@"flightsDB.csv");
            string line;
            file.ReadLine();
            int counter = 0;

            while ((line = file.ReadLine()) != null)
            {
                string[] flight = line.Split(',');

                string flightDate = flight[5];
                string uniqueCarrier = removeComillas(flight[6]);
                string airlineID = flight[7];
                flight[10] = removeComillas(flight[10]);
                int flightNum = Int32.Parse(flight[10]);
                string originAirport = flight[11];
                flight[15] = flight[15] + "," + flight[16];
                string originCityName = removeComillas(flight[15]);
                int destAirportID = Int32.Parse(flight[21]);
                flight[25] = flight[25] + "," + flight[26];
                string destCityName = removeComillas(flight[25]);
                string CRSDeptTime = removeComillas(flight[31]);
                string depTime = removeComillas(flight[32]);
                int depDelay;
                if (flight[33].Equals(""))
                {
                    depDelay = 0;
                }
                else {
                    depDelay = Convert.ToInt32(Convert.ToDouble(flight[33]));
                }
                string CRSArriveTime = removeComillas(flight[42]);
                int arrDelay;
                if (flight[44].Equals(""))
                {
                    arrDelay = 0;
                }
                else
                {
                    arrDelay = Convert.ToInt32(Convert.ToDouble(flight[44]));
                }
                int cancelled = Convert.ToInt32(Convert.ToDouble(flight[49]));
                int airTime;
                if (flight[54].Equals(""))
                {
                    airTime = 0;
                }
                else
                {
                    airTime = Convert.ToInt32(Convert.ToDouble(flight[54]));
                }
                

                Flight newFlight = new Flight(flightDate, uniqueCarrier, airlineID, flightNum, originAirport,
                    originCityName, destAirportID, destCityName, CRSDeptTime, depTime, depDelay, CRSArriveTime,
                    arrDelay, cancelled, airTime);
                counter++;
                usaFlights.Add("Vuelo "+counter+": ",newFlight);
                dt.Rows.Add(counter, flightNum, originCityName, destCityName, flightDate, depTime, CRSArriveTime, arrDelay);
            }
            dataGridView1.DataSource = dt;
            dataGridView1.Columns[3].Visible = false;
            dataGridView1.Columns[4].Visible = false;
            dataGridView1.Columns[5].Visible = false;
            dataGridView1.Columns[6].Visible = false;
            dataGridView1.Columns[7].Visible = false;
            file.Close();
        }

        public string removeComillas(string chain) {
            chain = chain.Remove(0,1);
            chain = chain.Remove(chain.Length - 1, 1);
            return chain;
        }

        private void SeleccionarRegistro(object sender, DataGridViewCellMouseEventArgs e)
        {
            poligono.Clear();
            puntos.Clear();

            filaSeleccionada = e.RowIndex;
            string value = dataGridView1.Rows[filaSeleccionada].Cells[0].Value.ToString();
            int valueInt = Int32.Parse(value);
            string originCityName = dataGridView1.Rows[filaSeleccionada].Cells[2].Value.ToString();
            string destCityName = dataGridView1.Rows[filaSeleccionada].Cells[3].Value.ToString();
            string flightDate = dataGridView1.Rows[filaSeleccionada].Cells[4].Value.ToString();
            string depTime = dataGridView1.Rows[filaSeleccionada].Cells[5].Value.ToString();
            string arrTime = dataGridView1.Rows[filaSeleccionada].Cells[6].Value.ToString();
            int arrDelay = Int32.Parse(dataGridView1.Rows[filaSeleccionada].Cells[7].Value.ToString());

            GeoCoderStatusCode statusCode;
            PointLatLng? pointLatLng1 = OpenStreet4UMapProvider.Instance.GetPoint(originCityName+",USA", out statusCode);
            PointLatLng? pointLatLng2 = OpenStreet4UMapProvider.Instance.GetPoint(destCityName + ",USA", out statusCode);

            markerOrigin.Position = new PointLatLng(pointLatLng1.Value.Lat,pointLatLng1.Value.Lng);
            markerDestination.Position = new PointLatLng(pointLatLng2.Value.Lat, pointLatLng2.Value.Lng);
            gMapControl.Overlays.Add(markerOverlay);
            gMapControl.Position = markerOrigin.Position;

            puntos.Add(new PointLatLng(pointLatLng1.Value.Lat, pointLatLng1.Value.Lng));
            puntos.Add(new PointLatLng(pointLatLng2.Value.Lat, pointLatLng2.Value.Lng));

            GMapPolygon poligonoPuntos = new GMapPolygon(puntos, "Poligono");
            poligono.Polygons.Add(poligonoPuntos);

            markerOrigin.ToolTipMode = MarkerTooltipMode.Always;
            markerOrigin.ToolTipText = string.Format("OriginCity: {0} \n DestinationCity: {1} \n FlightDate: {2} \n DepartureTime: {3} \n ArrivalTime: {4} \n ArrivalDelay: {5} minutes"
                ,originCityName,destCityName,flightDate,depTime,arrTime,arrDelay/100);

            gMapControl.Overlays.Add(poligono);

            gMapControl.Zoom = gMapControl.Zoom + 1;
            gMapControl.Zoom = gMapControl.Zoom - 1;
        }
    }
}
